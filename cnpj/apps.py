from django.apps import AppConfig


class CnpjConfig(AppConfig):
    name = 'cnpj'
